<?php session_start(); ?>
<?php require_once("../includes/db.php"); ?>
<?php require_once("../includes/function.php"); ?>
<?php if (isset($_SESSION["admin"])) { $_SESSION["admin"] = NULL; } ?>
<?php 
    if (isset($_REQUEST["login"])) {
        $fields = array("user","pass");
        
        $errors = array();
        foreach($fields as $field){
            if (empty($_REQUEST[$field])) {
                $errors[$field] = "<p class=\"message\">* ".ucfirst(remove_under_score($field)." is empty</p>");
             }
        }
        if (empty($errors)) {
            $username = mysql_prep(trim($_REQUEST["user"]));
            $password =trim(md5($_REQUEST["pass"]));
            $query = "SELECT * FROM admin WHERE username = '{$username}' AND hashed_password = '{$password}'";
            $result = mysqli_query($connection,$query);
            

            if ($result) {
              $admin = mysqli_fetch_assoc($result);
              if (isset($username,$password) && $username == $admin["username"] && $password == $admin["hashed_password"]) {
                  $_SESSION["admin"] = $admin["username"];
                  $_SESSION["message"] = "<p class=\"message_alert green\">You are successfully loged in</P>";
                  redirect_to("welcome.php");
              }else{
                $message = "<p class=\"message_alert red\">Username/Password Wrong!</P>";
              }
            }else{
                $message = "<p class=\"message_alert red\">Login query failed!</P>";
            }
        }else{
            #else what will happen....
        }
    }else{

    }

 ?>




<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
  <title>Photography</title>
  <meta name="description" content="free website template" />
  <meta name="keywords" content="enter your keywords here" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <!-- Custom Fonts -->
 
  <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
  <link href="../css/portfolio.css" rel="stylesheet" type="text/css" />
  <link href="../css/dark.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/jquery.galleriffic.js"></script>
  <script type="text/javascript" src="../js/jquery.opacityrollover.js"></script>
  <style>
    #site_content{margin-top:80px; width:800px;}
  </style>
</head>

<body>
  <div id="main">
  	<div id="site_content">	
      <h3 class="login_heading">login</h3>
      <?php if(isset($message)) {echo $message;} ?>
      <form class="form-horizontal" action="index.php" method="post">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-3 control-label">User Name</label>
          <div class="col-sm-9">
            <input type="text" name="user" class="form-control" id="inputEmail3" placeholder="User Name" value="<?php if (isset($_REQUEST["user"])){ echo $_REQUEST["user"];} ?>">
            <?php if(!empty($errors["user"])){echo $errors["user"];} ?>
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
          <div class="col-sm-9">
            <input type="password" name="pass" class="form-control" id="inputPassword3" placeholder="Password">
            <?php if(!empty($errors["pass"])){echo $errors["pass"];} ?>
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword4" class="col-sm-6 control-label"></label>
          <div class="col-sm-6">
            <button type="submit" name="login" class="btn btn-default">Submit</button>
          </div>
        </div>
      </form>
  	      
  	</div><!--end site_content-->
  </div><!--end main-->

  <footer id="footer">

      <div class="container">
        <div class="row myfooter">
          <div class=""><div class="">
          © Copyright Company 2015 | <a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a>
          </div></div>
          <div class="">
            <div class="">Designed by <a href="http://www.binary-logic.net"> Binary-Logic.net</a></div>
          </div>
        </div>
      </div>
    </footer>

    
  <script type="text/javascript" src="../js/jquery-1.11.2.min.js"></script>  
  <script type="text/javascript" src="../js/bootstrap.min.js"></script>  
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      // we only want these styles applied when javascript is enabled
      $('div.content').css('display', 'block');
      // initially set opacity on thumbs and add additional styling for hover effect on thumbs
      var onMouseOutOpacity = 0.67;
      $('#thumbs ul.thumbs li, div.navigation a.pageLink').opacityrollover({
        mouseOutOpacity:   onMouseOutOpacity,
        mouseOverOpacity:  1.0,
        fadeSpeed:         'fast',
        exemptionSelector: '.selected'
      });
      // initialize advanced galleriffic gallery
      var gallery = $('#thumbs').galleriffic({
        delay:                     3500,
        numThumbs:                 10,
        preloadAhead:              10,
        enableTopPager:            false,
        enableBottomPager:         false,
        imageContainerSel:         '#slideshow',
        controlsContainerSel:      '#controls',
        captionContainerSel:       '#caption',
        loadingContainerSel:       '#loading',
        renderSSControls:          true,
        renderNavControls:         true,
        playLinkText:              'Play Slideshow',
        pauseLinkText:             'Pause Slideshow',
        prevLinkText:              '&lsaquo; Previous Photo',
        nextLinkText:              'Next Photo &rsaquo;',
        nextPageLinkText:          'Next &rsaquo;',
        prevPageLinkText:          '&lsaquo; Prev',
        enableHistory:             true,
        autoStart:                 false,
        syncTransitions:           true,
        defaultTransitionDuration: 900,
        onSlideChange:             function(prevIndex, nextIndex) {
          // 'this' refers to the gallery, which is an extension of $('#thumbs')
          this.find('ul.thumbs').children()
            .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
            .eq(nextIndex).fadeTo('fast', 1.0);

          // update the photo index display
          this.$captionContainer.find('div.photo-index')
            .html('Photo '+ (nextIndex+1) +' of '+ this.data.length);
        },
        onPageTransitionOut:       function(callback) {
          this.fadeTo('fast', 0.0, callback);
        },
        onPageTransitionIn:        function() {
          var prevPageLink = this.find('a.prev').css('visibility', 'hidden');
          var nextPageLink = this.find('a.next').css('visibility', 'hidden');
          // show appropriate next / prev page links
          if (this.displayedPage > 0)
            prevPageLink.css('visibility', 'visible');
          var lastPage = this.getNumPages() - 1;
          if (this.displayedPage < lastPage)
            nextPageLink.css('visibility', 'visible');
          this.fadeTo('fast', 1.0);
        }
      });
      // event handlers for custom next / prev page links
      gallery.find('a.prev').click(function(e) {
        gallery.previousPage();
        e.preventDefault();
      });
      gallery.find('a.next').click(function(e) {
        gallery.nextPage();
        e.preventDefault();
      });
    });
  </script>
</body>
</html>
