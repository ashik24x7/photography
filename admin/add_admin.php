<?php session_start(); ?>
<?php require_once("../includes/db.php"); ?>
<?php require_once("../includes/function.php"); ?>
<?php if (!isset($_SESSION["admin"])) { redirect_to("index.php"); } ?>
<?php
if (isset($_REQUEST["add"])) {
        $fields = array("username","password");
        $errors = array();
        foreach($fields as $field){
            if (empty($_REQUEST[$field])) {
                $errors[$field] = "<p class=\"message\">* ".ucfirst(remove_under_score($field)." is empty</p>");
             }
        }
        
        if (empty($errors)) {
            $username = mysql_prep(trim($_REQUEST['username']));
            $password = mysql_prep(trim(md5($_REQUEST['password'])));
            $query = "INSERT INTO admin( ";
            $query .= "`username`, ";
            $query .= "`hashed_password` ";
            $query .= ")VALUES( "; 
            $query .= "'{$username}', ";
            $query .= "'{$password}' ";
            $query .=")";

            $result = mysqli_query($connection,$query);

            if ($result) {
                $_SESSION["message"] = "<p class=\"message_alert green\">New Admin has added sucessfully</P>";
                redirect_to("view_admin.php");
            }else{
                $message = "<p class=\"message_alert red\">New admin add was unsucessful</P>";
            }

        }else{
            #else what will happen....
        }
    }else{

  }

 ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
  <title>Exhibition</title>
  <meta name="description" content="free website template" />
  <meta name="keywords" content="enter your keywords here" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=9" />
  <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="../css/font.css">
  <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
  <link href="../css/portfolio.css" rel="stylesheet" type="text/css" />
  <link href="../css/dark.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/jquery.galleriffic.js"></script>
  <script type="text/javascript" src="../js/jquery.opacityrollover.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      // we only want these styles applied when javascript is enabled
      $('div.content').css('display', 'block');
      // initially set opacity on thumbs and add additional styling for hover effect on thumbs
      var onMouseOutOpacity = 0.67;
      $('#thumbs ul.thumbs li, div.navigation a.pageLink').opacityrollover({
        mouseOutOpacity:   onMouseOutOpacity,
        mouseOverOpacity:  1.0,
        fadeSpeed:         'fast',
        exemptionSelector: '.selected'
      });
      // initialize advanced galleriffic gallery
      var gallery = $('#thumbs').galleriffic({
        delay:                     3500,
        numThumbs:                 10,
        preloadAhead:              10,
        enableTopPager:            false,
        enableBottomPager:         false,
        imageContainerSel:         '#slideshow',
        controlsContainerSel:      '#controls',
        captionContainerSel:       '#caption',
        loadingContainerSel:       '#loading',
        renderSSControls:          true,
        renderNavControls:         true,
        playLinkText:              'Play Slideshow',
        pauseLinkText:             'Pause Slideshow',
        prevLinkText:              '&lsaquo; Previous Photo',
        nextLinkText:              'Next Photo &rsaquo;',
        nextPageLinkText:          'Next &rsaquo;',
        prevPageLinkText:          '&lsaquo; Prev',
        enableHistory:             true,
        autoStart:                 false,
        syncTransitions:           true,
        defaultTransitionDuration: 900,
        onSlideChange:             function(prevIndex, nextIndex) {
          // 'this' refers to the gallery, which is an extension of $('#thumbs')
          this.find('ul.thumbs').children()
            .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
            .eq(nextIndex).fadeTo('fast', 1.0);

          // update the photo index display
          this.$captionContainer.find('div.photo-index')
            .html('Photo '+ (nextIndex+1) +' of '+ this.data.length);
        },
        onPageTransitionOut:       function(callback) {
          this.fadeTo('fast', 0.0, callback);
        },
        onPageTransitionIn:        function() {
          var prevPageLink = this.find('a.prev').css('visibility', 'hidden');
          var nextPageLink = this.find('a.next').css('visibility', 'hidden');
          // show appropriate next / prev page links
          if (this.displayedPage > 0)
            prevPageLink.css('visibility', 'visible');
          var lastPage = this.getNumPages() - 1;
          if (this.displayedPage < lastPage)
            nextPageLink.css('visibility', 'visible');
          this.fadeTo('fast', 1.0);
        }
      });
      // event handlers for custom next / prev page links
      gallery.find('a.prev').click(function(e) {
        gallery.previousPage();
        e.preventDefault();
      });
      gallery.find('a.next').click(function(e) {
        gallery.nextPage();
        e.preventDefault();
      });
    });
  </script>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="system_management_heading">
       <h3>admin area</h3>
    </div><!--end welcome-->
      
    </div><!--end header-->
    
  <div id="site_content"> 
        <div class="system_management_section">
          <div class="container">
            <div class="row">
              <div class="col-md-3">
                <?php include("menu.php"); ?>
              </div>
              <div class="col-md-9">
                  <div class="slider_right_content">
                    <?php if(isset($message)){echo $message;}; ?>
                    <div class="form_section">
                      <form class="form-horizontal" action="add_admin.php" method="post">
                        <div class="form-group">
                          <label for="inputoldPass3" class="col-sm-5 control-label">Username: </label>
                          <div class="col-sm-7">
                           <input type="text" name="username" class="form-control" id="inputoldPass3" value="<?php if(isset($_REQUEST["username"])){ echo $_REQUEST["username"];} ?>">
                           <?php if(!empty($errors["username"])){echo $errors["username"];} ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputnewPass3" class="col-sm-5 control-label">Password: </label>
                          <div class="col-sm-7">
                           <input type="password" name="password" class="form-control" id="inputnewPass3">
                           <?php if(!empty($errors["password"])){echo $errors["password"];} ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputVisible4" class="col-sm-6 control-label"></label>
                          <div class="col-sm-6">
                            <button type="submit" name="add" class="btn btn-default pull-right">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
  </div><!--end site_content-->
  </div><!--end main-->

  <footer id="footer">

      <div class="container">
        <div class="row myfooter">
          <div class=""><div class="">
          © Copyright Company 2015 | <a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a>
          </div></div>
          <div class="">
            <div class="">Designed by <a href="http://www.binary-logic.net"> Binary-Logic.net</a></div>
          </div>
        </div>
      </div>
    </footer>

  <script type="text/javascript" src="../js/jquery-1.11.2.min.js"></script>  
  <script type="text/javascript" src="../js/bootstrap.min.js"></script>  
  
</body>
</html>
