-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2015 at 02:37 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `photography`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE IF NOT EXISTS `about` (
  `id` int(9) NOT NULL,
  `img_name` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `img_name`, `image`, `content`) VALUES
(1, 'About Image', 'sony_9635856568.png', 'Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula. Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim, pulvinar ac, lorem. Vestibulum sed ante. Donec sagittis euismod purus. Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula. Aliquam dapibus tincidunt metus.');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `hashed_password` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `hashed_password`) VALUES
(10, 'binary', 'adc442a7e35d6518d6cc6439332637f3'),
(11, 'abhi', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `biography`
--

CREATE TABLE IF NOT EXISTS `biography` (
  `id` int(9) NOT NULL,
  `heading` varchar(50) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biography`
--

INSERT INTO `biography` (`id`, `heading`, `content`) VALUES
(1, 'This is the heading', 'Mauris accumsan nulla vel diam. Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula. Aliquam dapibus tincidunt metus. Praesent justo dolor, lobortis quis, lobortis dignissim,');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `web` varchar(70) NOT NULL,
  `address` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `phone`, `email`, `web`, `address`) VALUES
(1, '+8801788314078', 'admininfo@photography.com', 'www.photography.com', 'Nikunja, Dhaka Bangladesh');

-- --------------------------------------------------------

--
-- Table structure for table `exhibition`
--

CREATE TABLE IF NOT EXISTS `exhibition` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `client` varchar(100) NOT NULL,
  `position` int(20) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `exhibition`
--

INSERT INTO `exhibition` (`id`, `comment`, `client`, `position`, `visible`) VALUES
(7, 'Sed in lacus ut enim adi piscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.', 'Linda Grey', 2, 1),
(10, 'Mauris accumsan nulla vel diam. Sed in lacus ut enim adi piscing aliquet. Nulla venenatis. In pede mi, aliquet sit amet, euismod in, auctor ut, ligula.', 'Dan Adams', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `heading` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `position` int(5) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `heading`, `content`, `position`, `visible`) VALUES
(1, 'Packages | Wedding Premium', 'Wedding Premium: BDT 55,000.00 LuthfulPlabon along with 3 other Photographers will cover the event Unlimited Photos will be taken Photographers will be at the event venue as long as the event continues Photographers will cover the Couple, Group Shots, Friends and Family 1 Slide Show of all the edited photographs Edited 300 copies 5R (5â€™â€™ * 7.5â€) Gloss/Matte Print Edited 3 copies 10L (10â€™â€™ * 15â€) Gloss/Matte Print Edited 2 copies 12L (12â€™â€™ * 18â€) Gloss/Matte Print All High Resolution images will be delivered in a DVD Special Offer with this Package An exclusive Pre-Wedding Photo shoot will be done by LuthfulPlabon at a location of the coupleâ€™s preference. Or, we can suggest beautiful locations around the city as well. This pre-wedding package of 3 hours is worth BDT 8,000 and it is complimentary with the Wedding Premium Package.', 1, 1),
(2, 'Lacinia est', 'Wedding Premium: BDT 55,000.00 LuthfulPlabon along with 3 other Photographers will cover the event Unlimited Photos will be taken Photographers will be at the event venue as long as the event continues Photographers will cover the Couple, Group Shots, Friends and Family 1 Slide Show of all the edited photographs Edited 300 copies 5R (5â€™â€™ * 7.5â€) Gloss/Matte Print Edited 3 copies 10L (10â€™â€™ * 15â€) Gloss/Matte Print Edited 2 copies 12L (12â€™â€™ * 18â€) Gloss/Matte Print All High Resolution images will be delivered in a DVD Special Offer with this Package An exclusive Pre-Wedding Photo shoot will be done by LuthfulPlabon at a location of the coupleâ€™s preference. Or, we can suggest beautiful locations around the city as well. This pre-wedding package of 3 hours is worth BDT 8,000 and it is complimentary with the Wedding Premium Package.', 2, 1),
(3, 'Proin dictum', 'Wedding Exclusive: BDT 45,000.00 Single Event Luthful Plabon along with 2 other Photographers will cover the event Unlimited Photos will be taken Photographers will be at the event venue as long as the event continues Photographers will cover the Couple, Group Shots, Friends and Family Edited 200 copies 5R (5â€™â€™ * 7.5â€) Gloss/Matte Print Edited 2 copies 10L (10â€™â€™ * 15â€) Gloss/Matte Print Edited 1 copy 12L (12â€™â€™ * 18â€) Gloss/Matte Print All High Resolution images will be delivered in a DVD', 3, 1),
(4, 'Elementum velit', 'Wedding Value: BDT 35,000.00 Single Event Luthful Plabon along with 1 other Photographer will cover the event Unlimited Photos will be taken Photographers will cover the Couple and Group Shots Edited 100 copies 5R (5â€™â€™ * 7.5â€) Gloss/Matte Print Edited 1 copy 10L (10â€™â€™ * 15â€) Gloss/Matte Print Edited 1 copy 12L (12â€™â€™ * 18â€) Gloss/Matte Print All High Resolution images will be delivered in a DVD', 4, 1),
(5, 'Packages | Pre/Post Wedding Photography', 'Premium Package: BDT 20,000 The Pre/Post wedding session is unique additions to our wedding packages. A team of to 2 Photographers lead by Luthful Plabon will be photographing the couple for 8 hours at a location of the clients choosing. Also, we can suggest beautiful locations around the city. Features of this package are as follows:', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `image` varchar(256) NOT NULL,
  `position` int(9) NOT NULL,
  `visible` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `name`, `image`, `position`, `visible`) VALUES
(17, 'img3', 'slider4.jpg.jpg', 3, 1),
(21, 'img2', 'slider3.jpg.jpg', 2, 1),
(29, 'img4', 'slider5.jpg.jpg', 4, 1),
(30, 'img5', 'slider6.jpg.jpg', 5, 1),
(31, 'img1', 'slider1.jpg.jpg', 1, 1),
(32, 'img6', 'slider7.jpg.jpg', 6, 1),
(33, 'img7', 'slider8.jpg.jpg', 7, 1),
(34, 'img8', 'slider10.jpg.jpg', 8, 1),
(35, 'img9', 'slider12.jpg.jpg', 9, 1),
(36, 'img10', 'slider11.jpg.jpg', 10, 1);

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE IF NOT EXISTS `social` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `facebook` varchar(250) NOT NULL,
  `twitter` varchar(250) NOT NULL,
  `google` varchar(250) NOT NULL,
  `linkedin` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`id`, `facebook`, `twitter`, `google`, `linkedin`) VALUES
(1, 'www.facebook.com', 'www.twitter.com', 'google', 'linkedin');

-- --------------------------------------------------------

--
-- Table structure for table `work`
--

CREATE TABLE IF NOT EXISTS `work` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  `image` varchar(250) NOT NULL,
  `catagory` varchar(50) NOT NULL,
  `position` int(9) NOT NULL,
  `visible` tinyint(70) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `work`
--

INSERT INTO `work` (`id`, `name`, `image`, `catagory`, `position`, `visible`) VALUES
(2, 'img1', '2.jpg_thumb.jpg', 'environmental', 4, 1),
(3, 'img2', 'devices.jpg_thumb.jpg', 'more', 2, 1),
(4, 'img3', '1.jpg_thumb.jpg', 'product', 3, 1),
(5, 'img4', '3.jpg_thumb.jpg', 'weeding', 5, 1),
(6, 'img5', 'unsplash1.jpg_thumb.jpg', 'weeding', 1, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
