<?php session_start(); ?>
<?php require_once("includes/db.php"); ?>
<?php require_once("includes/function.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
  <title>Photography</title>
  <meta name="description" content="free website template" />
  <meta name="keywords" content="enter your keywords here" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=9" />
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="css/font.css">
  <link href="css/style.css" rel="stylesheet" type="text/css" />
  <link href="css/portfolio.css" rel="stylesheet" type="text/css" />
  <link href="css/dark.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/jquery.galleriffic.js"></script>
  <script type="text/javascript" src="js/jquery.opacityrollover.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      // we only want these styles applied when javascript is enabled
      $('div.content').css('display', 'block');
      // initially set opacity on thumbs and add additional styling for hover effect on thumbs
      var onMouseOutOpacity = 0.67;
      $('#thumbs ul.thumbs li, div.navigation a.pageLink').opacityrollover({
        mouseOutOpacity:   onMouseOutOpacity,
        mouseOverOpacity:  1.0,
        fadeSpeed:         'fast',
        exemptionSelector: '.selected'
      });
      // initialize advanced galleriffic gallery
      var gallery = $('#thumbs').galleriffic({
        delay:                     3500,
        numThumbs:                 10,
        preloadAhead:              10,
        enableTopPager:            false,
        enableBottomPager:         false,
        imageContainerSel:         '#slideshow',
        controlsContainerSel:      '#controls',
        captionContainerSel:       '#caption',
        loadingContainerSel:       '#loading',
        renderSSControls:          true,
        renderNavControls:         true,
        playLinkText:              '<i class="fa fa-play"></i>',
        pauseLinkText:             '<i class="fa fa-pause"></i>',
        prevLinkText:              '',
        nextLinkText:              '',
        nextPageLinkText:          false,
        prevPageLinkText:          false,
        enableHistory:             true,
        autoStart:                 false,
        syncTransitions:           true,
        defaultTransitionDuration: 900,
        onSlideChange:             function(prevIndex, nextIndex) {
          // 'this' refers to the gallery, which is an extension of $('#thumbs')
          this.find('ul.thumbs').children()
            .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
            .eq(nextIndex).fadeTo('fast', 1.0);

          // update the photo index display
          this.$captionContainer.find('div.photo-index')
            .html('Photo '+ (nextIndex+1) +' of '+ this.data.length);
        },
        onPageTransitionOut:       function(callback) {
          this.fadeTo('fast', 0.0, callback);
        },
        onPageTransitionIn:        function() {
          var prevPageLink = this.find('a.prev').css('visibility', 'hidden');
          var nextPageLink = this.find('a.next').css('visibility', 'hidden');
          // show appropriate next / prev page links
          if (this.displayedPage > 0)
            prevPageLink.css('visibility', 'visible');
          var lastPage = this.getNumPages() - 1;
          if (this.displayedPage < lastPage)
            nextPageLink.css('visibility', 'visible');
          this.fadeTo('fast', 1.0);
        }
      });
      // event handlers for custom next / prev page links
      gallery.find('a.prev').click(function(e) {
        gallery.previousPage();
        e.preventDefault();
      });
      gallery.find('a.next').click(function(e) {
        gallery.nextPage();
        e.preventDefault();
      });
    });
  </script>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="welcome">
    </div><!--end welcome-->
      <div id="menubar">
        <ul id="menu">
          <li><a href="index.php#home">Home</a></li>
          <li><a href="index.php#about">About</a></li>
          <li class="dropdown">
                  <a href="index.php#work" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">works <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="enviormental.php">Environmental</a></li>
                    <li><a href="weeding.php">Weeding</a></li>
                    <li><a href="product.php">Product</a></li>
                    <li><a href="more.php">More</a></li>
                  </ul>
                </li>
          <li><a href="index.php#contact">Contact</a></li>
        </ul>
      </div><!--end menubar-->
    </div><!--end header-->
    
  <div id="site_content"> 
    <h3 class="enviormental_heading">More</h3>
    <div id="image_container">  
      <!-- start gallery HTML containers -->
      <div class="navigation-container">
        <div id="thumbs" class="navigation">
          <a class="pageLink prev" style="visibility: hidden;" href="#" title="Previous Page"></a>
          <ul class="thumbs noscript">
          <?php 
            $query = "SELECT * FROM work WHERE catagory= 'more' ORDER BY position DESC ";
            $result = mysqli_query($connection,$query);
            while ($work = mysqli_fetch_assoc($result)) {
          ?>  
            <li>
              <a class="thumb" href="admin/upload/work/<?php echo $work["image"]; ?>"><img src="admin/upload/work/<?php echo $work["image"]; ?>"></a>
              <div class="caption">
                <div class="image-title portfolio_two"><?php echo $work["name"]; ?></div>
              </div>
            </li>
          <?php }?>
          <?php mysqli_free_result($result); ?>
          </ul>
          <a class="pageLink next" style="visibility: hidden;" href="#" title="Next Page"></a>
        </div>
      </div>
      <div class="content">
        <div class="slideshow-container">
          <div id="loading" class="loader"></div>
          <div id="slideshow" class="slideshow"></div>
          <div id="controls" class="controls portfolio_two"></div>
          <div id="caption" class="caption-container"></div>
        </div>
      </div>
      <!-- end gallery HTML containers -->   
  </div><!--end image_container-->    
  </div><!--end site_content-->
  </div><!--end main-->

  <footer id="footer">

      <div class="container">
        <div class="row myfooter">
          <div class="col-sm-6"><div class="">
          © Copyright Company 2015 | <a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a>
          </div></div>
          <div class="col-sm-6">
            <div class="">Designed by <a href="http://www.binary-logic.net"> Binary-Logic.net</a></div>
          </div>
        </div>
      </div>
    </footer>

   
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>
</html>
