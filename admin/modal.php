<!-- Modal -->
                                <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                  <div class="modal-dialog" role="document" style="width:80%">
                                    <div class="modal-content"style="width:70%">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Service</h4>
                                      </div>
                                      <div class="modal-body">
                                          <form class="form-horizontal" action="add_slider_img.php" method="post" enctype="multipart/form-data">
                                            <div class="form-group">
                                              <label for="inputName3" class="col-sm-3 control-label">Name: </label>
                                              <div class="col-sm-9">
                                                <input type="text" name="img_name" class="form-control" id="inputName3" placeholder="Name">
                                                <?php if(!empty($errors["img_name"])){echo $errors["img_name"];} ?>
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="col-sm-3 control-label">Image: </label>
                                              <div class="col-sm-9">
                                                <input type="file" name="img" class="form-control">
                                                <?php if(!empty($errors["img"])){echo $errors["img"];} ?>
                                              </div>
                                            </div>
                                             
                                             <div class="form-group">
                                              <label for="inputPosition3" class="col-sm-3 control-label">Position: </label>
                                              <div class="col-sm-9">
                                                <select name="pos" class="form-control">
                                                  <option value=""></option>
                                                  <option value="1">1</option>
                                                  <option value="2">2</option>
                                                </select>
                                                <?php if(!empty($errors["pos"])){echo $errors["pos"];} ?>
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label for="inputVisible3" class="col-sm-3 control-label">Visible: </label>
                                              <div class="col-sm-9">
                                                <select name="visible" class="form-control">
                                                  <option value="">Select</option>
                                                  <option value="1">Yes</option>
                                                  <option value="0">No</option>
                                                </select>
                                                <?php if(!empty($errors["visible"])){echo $errors["visible"];} ?>
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label for="inputVisible4" class="col-sm-6 control-label"></label>
                                              <div class="col-sm-6">
                                                <button type="submit" name="add" class="btn btn-default pull-right">&nbsp;&nbsp;Add&nbsp;&nbsp;</button>
                                              </div>
                                            </div>
                                          </form>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
                                      </div>
                                    </div>
                                  </div>
                                </div>