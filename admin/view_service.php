<?php session_start(); ?>
<?php require_once("../includes/db.php"); ?>
<?php require_once("../includes/function.php"); ?>
<?php if (!isset($_SESSION["admin"])) { redirect_to("index.php"); } ?>
<?php
    $item_per_page = 5;
    $total_item = mysqli_num_rows(mysqli_query($connection,"SELECT * FROM service"));
    $pages = ceil($total_item/$item_per_page);
    $page = isset($_GET["page"]) ? (int)$_GET["page"] : $_GET["page"] = 1;
    $start = (($page - 1)*$item_per_page);

    $query = "SELECT * FROM service ORDER BY position DESC LIMIT {$start},{$item_per_page}";
    $result = mysqli_query($connection,$query);
    
    


 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
  <title>Photography</title>
  <meta name="description" content="free website template" />
  <meta name="keywords" content="enter your keywords here" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=9" />
  <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="../css/font.css">
  <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
  <link href="../css/portfolio.css" rel="stylesheet" type="text/css" />
  <link href="../css/dark.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/jquery.galleriffic.js"></script>
  <script type="text/javascript" src="../js/jquery.opacityrollover.js"></script>
  <script type="text/javascript">
    jQuery(document).ready(function($) {
      // we only want these styles applied when javascript is enabled
      $('div.content').css('display', 'block');
      // initially set opacity on thumbs and add additional styling for hover effect on thumbs
      var onMouseOutOpacity = 0.67;
      $('#thumbs ul.thumbs li, div.navigation a.pageLink').opacityrollover({
        mouseOutOpacity:   onMouseOutOpacity,
        mouseOverOpacity:  1.0,
        fadeSpeed:         'fast',
        exemptionSelector: '.selected'
      });
      // initialize advanced galleriffic gallery
      var gallery = $('#thumbs').galleriffic({
        delay:                     3500,
        numThumbs:                 10,
        preloadAhead:              10,
        enableTopPager:            false,
        enableBottomPager:         false,
        imageContainerSel:         '#slideshow',
        controlsContainerSel:      '#controls',
        captionContainerSel:       '#caption',
        loadingContainerSel:       '#loading',
        renderSSControls:          true,
        renderNavControls:         true,
        playLinkText:              'Play Slideshow',
        pauseLinkText:             'Pause Slideshow',
        prevLinkText:              '&lsaquo; Previous Photo',
        nextLinkText:              'Next Photo &rsaquo;',
        nextPageLinkText:          'Next &rsaquo;',
        prevPageLinkText:          '&lsaquo; Prev',
        enableHistory:             true,
        autoStart:                 false,
        syncTransitions:           true,
        defaultTransitionDuration: 900,
        onSlideChange:             function(prevIndex, nextIndex) {
          // 'this' refers to the gallery, which is an extension of $('#thumbs')
          this.find('ul.thumbs').children()
            .eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
            .eq(nextIndex).fadeTo('fast', 1.0);

          // update the photo index display
          this.$captionContainer.find('div.photo-index')
            .html('Photo '+ (nextIndex+1) +' of '+ this.data.length);
        },
        onPageTransitionOut:       function(callback) {
          this.fadeTo('fast', 0.0, callback);
        },
        onPageTransitionIn:        function() {
          var prevPageLink = this.find('a.prev').css('visibility', 'hidden');
          var nextPageLink = this.find('a.next').css('visibility', 'hidden');
          // show appropriate next / prev page links
          if (this.displayedPage > 0)
            prevPageLink.css('visibility', 'visible');
          var lastPage = this.getNumPages() - 1;
          if (this.displayedPage < lastPage)
            nextPageLink.css('visibility', 'visible');
          this.fadeTo('fast', 1.0);
        }
      });
      // event handlers for custom next / prev page links
      gallery.find('a.prev').click(function(e) {
        gallery.previousPage();
        e.preventDefault();
      });
      gallery.find('a.next').click(function(e) {
        gallery.nextPage();
        e.preventDefault();
      });
    });
  </script>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="system_management_heading">
       <h3>admin area</h3>
    </div><!--end welcome-->
      
    </div><!--end header-->
    
  <div id="site_content"> 
        <div class="system_management_section">
          <div class="container">
            <div class="row">
              <div class="col-md-3">
                <?php include("menu.php"); ?>
              </div>
              <div class="col-md-9">
                  <div class="slider_right_content">
                    <div class="message">
                      <?php if(isset($_SESSION["message"])){echo $_SESSION["message"];$_SESSION["message"] = NULL; } ?>
                    </div>
                    <div class="table_section col-md-12">
                       <table class="table table-striped">
                        
                        <tr>
                            <th class="active">Heading</th>
                            <th class="active">Content</th>
                            <th class="active">Position</th>
                            <th class="active">Visible</th>
                            <th class="active">Edit</th>
                            <th class="active">Delete</th>
                          </tr>
                          <!-- On rows -->
                          <?php 
                            if ($result) {
                              while ($service = mysqli_fetch_assoc($result)) {
                          ?>
                          <tr>
                            <td class="warning"><?php echo $service["heading"]?></td>
                            <td class="danger"><?php echo substr($service["content"],0,70); ?></td>
                            <td class="success"><?php echo $service["position"]?></td>
                            <td class="warning"><?php if($service["visible"]==1){echo "Yes";}else{ echo "No";}?></td>
                            <td class="info"><a href="update_service.php?id=<?php echo $service["id"]; ?>"><button type="button" class="btn btn-info">Edit</button></a></td>

                            <td class="danger"><a href="delete_service.php?id=<?php echo $service["id"]; ?>"><button type="button" class="btn btn-danger" onClick="return confirm('Are you sure?')">Delete</button></a></td>
                          </tr>
                          <?php
                            # code...
                          }
                        }

                     ?> 
                      </table>
                        <div class="fagetnation">
                           <nav>
                              <ul class="pagination pagination-center">
                                <li class="disabled">
                                  <a href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                  </a>
                                </li>
                                <?php 
                                  for($i=1;$i<=$pages;$i++){
                                    if($page == $i){
                                      echo "<li><a class=\"active\" href=\"view_service.php?page={$i}\">{$i}</a></li>";
                                    }else{
                                      echo "<li><a href=\"view_service.php?page={$i}\">{$i}</a></li>";
                                    }
                                  }

                                 ?>
                                <li class="disabled">
                                  <a href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                  </a>
                                </li>
                              </ul>
                            </nav>
                        </div>

                  </div>
              </div>
            </div>
          </div>
        </div>
  </div><!--end site_content-->
  </div><!--end main-->

  <footer id="footer">

      <div class="container">
        <div class="row myfooter">
          <div class=""><div class="">
          © Copyright Company 2015 | <a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a>
          </div></div>
          <div class="">
            <div class="">Designed by <a href="http://www.binary-logic.net"> Binary-Logic.net</a></div>
          </div>
        </div>
      </div>
    </footer>

  <script type="text/javascript" src="../js/jquery-1.11.2.min.js"></script>  
  <script type="text/javascript" src="../js/bootstrap.min.js"></script>  
  
</body>
</html>
