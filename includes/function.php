
<?php 
	function redirect_to($link){
		header("Location: ".$link);
		exit();
	}

	function remove_under_score($str){
		return str_replace("_", " ", $str);
	}

	function mysql_prep($string){
		global $connection;
		return mysqli_real_escape_string($connection,$string);
	}
 ?>