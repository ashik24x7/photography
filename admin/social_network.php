<?php session_start(); ?>
<?php require_once("../includes/db.php"); ?>
<?php require_once("../includes/function.php"); ?>
<?php if (!isset($_SESSION["admin"])) { redirect_to("index.php"); } ?>
<?php
     
    $query = "SELECT * FROM social";
    $result = mysqli_query($connection,$query);
    $social = mysqli_fetch_assoc($result);
?>
<?php
if (isset($_REQUEST["update"])) {
        $id = mysql_prep($_REQUEST["id"]);
        $fields = array("facebook","twitter","google","linkedin");
        $errors = array();
        foreach($fields as $field){
            if (empty($_REQUEST[$field])) {
                $errors[$field] = "<p class=\"message\">* ".ucfirst(remove_under_score($field)." is empty</p>");
             }
        }
        if (empty($errors)) {
            $facebook = urlencode(trim(mysql_prep($_REQUEST['facebook'])));
            $twitter = urlencode(trim(mysql_prep($_REQUEST['twitter'])));
            $google = urlencode(trim(mysql_prep($_REQUEST['google'])));
            $linkedin = urlencode(trim(mysql_prep($_REQUEST['linkedin'])));
            $query = "UPDATE social SET "; 
            $query .="facebook='{$facebook}', ";
            $query .="twitter='{$twitter}', ";
            $query .="google='{$google}', ";
            $query .="linkedin='{$linkedin}' ";
            $query .= "WHERE id={$id}";

            $result = mysqli_query($connection,$query);


            if ($result) {
                $_SESSION["message"] = "<p class=\"message_alert green\">Social item has been updated sucessfully</P>";
                redirect_to("social_network.php?id={$id}");
            }else{
                $message = "<p class=\"message_alert red\">Social item doesn't updated sucessfully</P>";
            }

        }else{
            #else what will happen....
        }
    }else{

  }

 ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
  <title>Exhibition</title>
  <meta name="description" content="free website template" />
  <meta name="keywords" content="enter your keywords here" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=9" />
  <link href="../font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="../css/font.css">
  <link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="../css/style.css" rel="stylesheet" type="text/css" />
  <link href="../css/portfolio.css" rel="stylesheet" type="text/css" />
  <link href="../css/dark.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="../js/jquery.min.js"></script>
  <script type="text/javascript" src="../js/jquery.galleriffic.js"></script>
  <script type="text/javascript" src="../js/jquery.opacityrollover.js"></script>
  
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="system_management_heading">
	     <h3>admin area</h3>
	  </div><!--end welcome-->
      
    </div><!--end header-->
    
	<div id="site_content">	
	      <div class="system_management_section">
          <div class="container">
            <div class="row">
              <div class="col-md-3">
                <?php include("menu.php"); ?>
              </div>
              <div class="col-md-9">
                  <div class="slider_right_content">
                    <h3 class="content_heading">Social-Network Section</h3>
                   <?php if(isset($message)){echo $message;}; ?>
                    <?php if(isset($_SESSION["message"])){echo $_SESSION["message"];$_SESSION["message"] = NULL; } ?>
                      <div class="form_section">
                      <form class="form-horizontal" action="social_network.php?id=1" method="post">
                        <div class="form-group">
                          <label for="inputfacebook3" class="col-sm-5 control-label"><i class="fa fa-facebook social-icons"></i></label>
                          <div class="col-sm-7">
                           <input type="text" name="facebook" class="form-control" id="inputfacebook3" value="<?php if (isset($social["facebook"])) { echo $social["facebook"]; } ?>">
                           <?php if(!empty($errors["facebook"])){echo $errors["facebook"];} ?> 
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputtwitter3" class="col-sm-5 control-label"><i class="fa fa-twitter social-icons"></i></label>
                          <div class="col-sm-7">
                           <input type="text" name="twitter" class="form-control" id="inputtwitter3" value="<?php if (isset($social["twitter"])) { echo $social["twitter"]; } ?>">
                           <?php if(!empty($errors["twitter"])){echo $errors["twitter"];} ?> 
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputgoogle3" class="col-sm-5 control-label"><i class="fa fa-google-plus social-icons" style="color:#F9263E"></i></label>
                          <div class="col-sm-7">
                           <input type="text" name="google" class="form-control" id="inputgoogle3" value="<?php if (isset($social["google"])) { echo $social["google"]; } ?>">
                            <?php if(!empty($errors["google"])){echo $errors["google"];} ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="inputlink3" class="col-sm-5 control-label"><i class="fa fa-linkedin social-icons"></i></label>
                          <div class="col-sm-7">
                           <input type="text" name="linkedin" class="form-control" id="inputlink3" value="<?php if (isset($social["linkedin"])) { echo $social["linkedin"]; } ?>">
                           <?php if(!empty($errors["linkedin"])){echo $errors["linkedin"];} ?> 
                          </div>
                        </div>
                        
                        
                        
                        
                        
                        <div class="form-group">
                          <label for="inputVisible4" class="col-sm-6 control-label"></label>
                          <div class="col-sm-6">
                            <button type="submit" name="update" class="btn btn-default pull-right">&nbsp;&nbsp;Update&nbsp;&nbsp;</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
	</div><!--end site_content-->
  </div><!--end main-->

  <footer id="footer">

      <div class="container">
        <div class="row myfooter">
          <div class=""><div class="">
          © Copyright Company 2015 | <a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a>
          </div></div>
          <div class="">
            <div class="">Designed by <a href="http://www.binary-logic.net"> Binary-Logic.net</a></div>
          </div>
        </div>
      </div>
    </footer>

  <script type="text/javascript" src="../js/jquery-1.11.2.min.js"></script>  
  <script type="text/javascript" src="../js/bootstrap.min.js"></script>  
  
</body>
</html>
