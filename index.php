<?php session_start(); ?>
<?php require_once("includes/db.php"); ?>
<?php require_once("includes/function.php"); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Photography</title>
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom Fonts -->
		<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="css/font.css">
		<link rel="stylesheet" type="text/css" href="css/ardestine.css">
		<!-- Custom CSS -->
		<link href="css/nivo-lightbox.css" rel="stylesheet">
		<link href="css/nivo-lightbox-theme/default/default.css" rel="stylesheet">
		<!-- <link rel="icon" type="image/png" href="images/icon-sm.png"> -->
		<link rel="stylesheet" href="css/patros.css" >
		<link rel="stylesheet" href="css/responsive.css" >
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
	</head>

	<body data-spy="scroll">
		<!-- Navigation -->
		<nav class="navbar navbar-fixed-top">
			<div class="container navbar-inverse">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right custom-menu">
						<li class="active"><a href="#home">Home</a></li>
						<li><a href="#about">About</a></li>
						<li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">works <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="enviormental.php">Environmental</a></li>
				            <li><a href="weeding.php">Weeding</a></li>
				            <li><a href="product.php">Product</a></li>
				            <li><a href="more.php">More</a></li>
				          </ul>
				        </li>
						
						<li><a href="#contact">Contact</a></li>
						<!-- <li><a href="blog.html">Blog</a></li>
						<li><a href="single-post.html">Single</a></li> -->
					</ul>
				</div>
			</div>
		</nav>

		<!-- Header Carousel -->
		<header id="home" class="carousel slide">
			<div class="container no_padding">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="6"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="7"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="8"></li>
			    <li data-target="#carousel-example-generic" data-slide-to="9"></li>
			  </ol>
			  <?php 
			  	$query = "SELECT * FROM slider ORDER BY position ASC";
			  	$result = mysqli_query($connection,$query);
				$slider = mysqli_fetch_assoc($result)
			  ?>
			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
			      <img src="admin/upload/<?php echo $slider["image"]; ?>" alt="<?php echo $slider["name"]; ?>">
			      <div class="carousel-caption">
			      </div>
			    </div>

			    <?php while ($slider = mysqli_fetch_assoc($result)) { ?>
			    <div class="item">
			      <img src="admin/upload/<?php echo $slider["image"]; ?>" alt="<?php echo $slider["name"]; ?>">
			      <div class="carousel-caption">
			      </div>
			    </div>
			    <?php } ?>
			    <?php mysqli_free_result($result); ?>
			  </div>

			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
			    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
			    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
			</div>         
		</header>
			
		<!-- Page Content -->
		<section id="about">
			<div class="container ">
				<div class="row about_container">
					<div class="col-md-12">
						<div class="text-center">
							<h2 style="font-family:oet">About Us</h2>
							<img class="img-responsive displayed" src="images/short.png" alt="Company about"/>
							<div class="row">
								<div class="col-md-12">
									<div class="row">
										<div class="col-md-6">
											<div class="about_camera_img">
											<?php 
											  $query = "SELECT * FROM about WHERE id=1";
											  $result = mysqli_query($connection,$query);
											  $about = mysqli_fetch_assoc($result);
											 ?>
												<img src="admin/upload/<?php echo $about["image"] ?>" alt="<?php echo $about["img_name"] ?>">
											</div>
										</div>
										<div class="col-md-6">
											<div class="about_camera_content">
												
												<p>
													<?php echo $about["content"] ?>
												</p>
											</div>
											<?php mysqli_free_result($result); ?>
										</div><!--  -->
									</div><!--  -->
									<div class="row">
										<div class="col-md-4">
											<div class="biography_section">
												<h3>biography</h3>
												<?php 
												  $query = "SELECT * FROM biography WHERE id=1";
												  $result = mysqli_query($connection,$query);
												  $biography = mysqli_fetch_assoc($result);
												 ?>
												<h3><?php echo $biography["heading"] ?></h3>
												<p><?php echo $biography["content"] ?></p>
												<?php mysqli_free_result($result); ?>
											</div>
										</div><!--  -->
										<div class="col-md-4">
											<div class="testimonials_section">
												<h3>Exhibition</h3>
												<?php 
												  $query = "SELECT * FROM exhibition WHERE visible=1 ORDER BY position DESC ";
												  $result = mysqli_query($connection,$query);
												  while ($exhibition = mysqli_fetch_assoc($result)) {
												  	echo "<p>“".$exhibition["comment"]."”</p>";
												  	echo "<p>- ".$exhibition["client"]."</p>";
												  }
												 ?>
											</div>
											<?php mysqli_free_result($result); ?>
										</div><!--  -->
										<div class="col-md-4">
											<div class="services_section">
												<h3>services</h3>
												<ul>
												<?php 
												  $query = "SELECT * FROM service WHERE visible=1 ORDER BY position DESC ";
												  $result = mysqli_query($connection,$query);
												  while ($service = mysqli_fetch_assoc($result)) {
												?>	
													<li><a href="services.php"><?php echo $service["heading"]; ?></a></li>
												 <?php }?>
												</ul>
												<?php mysqli_free_result($result); ?>
											</div>
										</div><!--  -->
									</div>
									
								</div>
							</div>
						</div>
					</div><!--  -->
				</div>
			</div>
		</section>


		<section id="work">
			<div class="container">
				<div class="row work_section">
					<div class="text-center">
					<h2 style="color:#fff; font-family:oet">Work</h2>
					<img class="img-responsive displayed" src="images/short.png" alt="about">
					</div>

					<div class="row">
						<div class="work_section_content">
							<div class="col-md-6">
							<div class="work_gallery">
								<!-- <img src="images/work/skeelo_white_tshirt.jpg" alt="gallery"> -->
								<p class="p2">Environmental</p>
								<a href="enviormental.php" class="btn add_see_more">
									
									View More
									<!-- <div class="transparent_overlay"></div> -->
								</a>
								
							</div>
							</div>
							<div class="col-md-6">
								<div class="work_gallery">
								<!-- <img src="images/work/alfred_sisley_nature_quote_tile_coaster.jpg" alt="gallery"> -->
								<p class="p2">Weeding</p>
								<a href="weeding.php" class="btn add_see_more">
									
									View More
									<!-- <div class="transparent_overlay"></div> -->
								</a>
								
							</div>
							</div>
							<div class="col-md-6">
								<div class="work_gallery">
								<!-- <img src="images/work/justin-biebe-pyd-shirtess.jpg" alt="gallery"> -->
								<p class="p2">Product</p>
								<a href="product.php" class="btn add_see_more">
									
									View More
									<!-- <div class="transparent_overlay"></div> -->
								</a>
								
							</div>
							</div>
							<div class="col-md-6">
								<div class="work_gallery">
								<!-- <img src="images/work/thepolobar.jpeg" alt="gallery"> -->
								<p class="p2">More</p>
								<a href="more.php" class="btn add_see_more">
									
									View More
									<!-- <div class="transparent_overlay"></div> -->
								</a>
								
							</div>
							</div>
						</div>
					</div>
		 			
		 				
				  </div> 
			</div>
		</section><!--  -->

		

		<section id="contact">
			<div class="container"> 
				<div class="row contact_section">
					<div class="col-md-12">
						<div class="col-lg-12">
							<div class="text-center color-elements">
							<h2 style="font-family:oet">Contact Us</h2>
							<img class="img-responsive displayed" src="images/short.png" alt="contact">
							</div>
						</div>
						

						<div class="col-md-6">

							<h3 class="contact_subheading">contact info</h3>
							<ul class="contact_info">
								<?php 
								  $query = "SELECT * FROM contact WHERE id=1";
								  $result = mysqli_query($connection,$query);
								  $contact = mysqli_fetch_assoc($result);
								?>
								<li><i class="fa fa-phone phone"></i> <h4><?php echo $contact["phone"]; ?></h4></li>
								<li><i class="fa fa-envelope envelope"></i><h4> <a href="mailto:<?php echo $contact["email"]; ?>"><?php echo $contact["email"]; ?></a></h4></li>
								<li><i class="fa fa-globe globe"></i> <h4><?php echo $contact["web"]; ?></h4></li>
								<li><i class="fa fa-map-marker marker"></i> <h4><?php echo $contact["address"]; ?></h4></li>
							</ul>

						</div><!--  -->
						<div class="col-md-6 form-section">
							<form class="form">
							  <div class="form-group col-sm-6 no_padding">
							    <label class="sr-only" for="exampleInputname3">Name</label>
							    <input name="name" type="text" class="form-control" id="exampleInputname3" placeholder="Enter Your Name">
							  </div>
							  <div class="form-group col-sm-6 no_padding">
							    <label class="sr-only" for="exampleInputEmail3">Email address</label>
							    <input name="sender_email" type="email" class="form-control" id="exampleInputEmail3" placeholder="Enter Your Email Address">
							  </div>
							  <div class="form-group col-sm-12 no_padding">
							    <label class="sr-only" for="exampleInputsubject3">Subject</label>
							    <input name="subject" type="text" class="form-control" id="exampleInputsubject3" placeholder="Enter Your Subject">
							  </div>
							  <div class="form-group">
							    <textarea name="message" class="form-control" rows="5" placeholder="Enter Your Message...."></textarea>
							  </div>





							  <div class="form-group col-sm-6 no_padding">
							  	<input type="text" name="verif_box" id="verif_box"class="form-control" placeholder="Write The Number"/>
							  </div>

							  <div class="form-group col-sm-3 no_padding">
							  	<img class="verifi_img" src="verificationimage.php?<?php echo rand(0,9999);?>" alt="verification image, type it in the box" align="absbottom" width="50" height="25" />
							  </div>

							  <div class="col-sm-12">
											<!-- if the variable "wrong_code" is sent from previous page then display the error field -->
									<?php if(isset($_GET['wrong_code'])){?>
									<div class="navbar-left">Wrong verification code !</div><br /> 
									<?php ;}?>
							  </div>


							   

							  
							  <button type="submit" class="btn send navbar-right">Send</button>
							</form>
						</div><!--  -->


					</div>
				</div>
			</div>
		</section>

		<section id="follow_us">
			<div class="container"> 
				<div class="row">
					<div class="col-md-12 follow_content">
					
							<div class="text-center height-contact-element">
							<h2 style="font-family:oet">Follow Us</h2>
							</div>
							<img class="img-responsive displayed" src="images/short.png" alt="short" />
							<div class="text-center height-contact-element">
								<ul class="list-unstyled list-inline list-social-icons">
									<li class="active"><a href="#"><i class="fa fa-facebook social-icons"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter social-icons"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus social-icons"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin social-icons"></i></a></li>
								</ul>
							</div>
						
					</div>
				</div>
			</div>
		</section>

		<footer id="footer">

			<div class="container">
				<div class="row myfooter">
					<div class="col-sm-6"><div class="pull-left">
					© Copyright Company 2015 | <a href="#">Privacy Policy</a> | <a href="#">Terms of Use</a>
					</div></div>
					<div class="col-sm-6">
						<div class="pull-right">Designed by <a href="http://www.binary-logic.net"> Binary-Logic.net</a></div>
					</div>
				</div>
			</div>
		</footer>

		<!-- jQuery -->
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/jquery.js"></script>
		<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
		<script src="js/nivo-lightbox.min.js"></script>

		<!-- Google Map -->
		<script src="//maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=true&amp;libraries=places"></script>

		<!-- Portfolio -->
		<script src="js/jquery.quicksand.js"></script>	
		<script type="text/javascript">
			$('.collapse').collapse();
		</script>
		
		

		
		<script type="text/javascript">
    	$(function() {

    	//nivo lightbox
		$('.gallery-item a').nivoLightbox({
			effect: 'fadeScale',                             // The effect to use when showing the lightbox
			theme: 'default',                           // The lightbox theme to use
			keyboardNav: true,                          // Enable/Disable keyboard navigation (left/right/escape)
			clickOverlayToClose: true,                  // If false clicking the "close" button will be the only way to close the lightbox
			onInit: function(){},                       // Callback when lightbox has loaded
			beforeShowLightbox: function(){},           // Callback before the lightbox is shown
			afterShowLightbox: function(lightbox){},    // Callback after the lightbox is shown
			beforeHideLightbox: function(){},           // Callback before the lightbox is hidden
			afterHideLightbox: function(){},            // Callback after the lightbox is hidden
			onPrev: function(element){},                // Callback when the lightbox gallery goes to previous item
			onNext: function(element){},                // Callback when the lightbox gallery goes to next item
			errorMessage: 'The requested content cannot be loaded. Please try again later.' // Error message when content can't be loaded
		});
	});

    </script>
<!-- end lightbox -->

	

		<!--Jquery Smooth Scrolling-->
		<script>
			$(document).ready(function(){
				$('.custom-menu a[href^="#"], .intro-scroller .inner-link').on('click',function (e) {
					e.preventDefault();

					var target = this.hash;
					var $target = $(target);

					$('html, body').stop().animate({
						'scrollTop': $target.offset().top
					}, 900, 'swing', function () {
						window.location.hash = target;
					});
				});

				$('a.page-scroll').bind('click', function(event) {
					var $anchor = $(this);
					$('html, body').stop().animate({
						scrollTop: $($anchor.attr('href')).offset().top
					}, 1500, 'easeInOutExpo');
					event.preventDefault();
				});

			   $(".nav a").on("click", function(){
					 $(".nav").find(".active").removeClass("active");
					$(this).parent().addClass("active");
				});

				$('body').append('<div id="toTop" class="btn btn-primary color1"><span class="glyphicon glyphicon-chevron-up"></span></div>');
					$(window).scroll(function () {
						if ($(this).scrollTop() != 0) {
							$('#toTop').fadeIn();
						} else {
							$('#toTop').fadeOut();
						}
					}); 
				$('#toTop').click(function(){
					$("html, body").animate({ scrollTop: 0 }, 700);
					return false;
				});

			});

		
		$('#myModal').on('shown.bs.modal', function () {
		  $('#myInput').focus()
		});



		</script>

		<script>
		function gallery(){};

		var $itemsHolder = $('ul.port2');
		var $itemsClone = $itemsHolder.clone(); 
		var $filterClass = "";
		$('ul.filter li').click(function(e) {
		e.preventDefault();
		$filterClass = $(this).attr('data-value');
			if($filterClass == 'all'){ var $filters = $itemsClone.find('li'); }
			else { var $filters = $itemsClone.find('li[data-type='+ $filterClass +']'); }
			$itemsHolder.quicksand(
			  $filters,
			  { duration: 1000 },
			  gallery
			  );
		});

		$(document).ready(gallery);
		</script>


</body>
</html>




